package jClassDesigner.test_bed;

import jClassDesigner.Constants;
import jClassDesigner.JClassDesigner;
import jClassDesigner.data.ComponentUI_DataWrapper;
import jClassDesigner.data.DataManager;
import jClassDesigner.data.Method_DataWrapper;
import jClassDesigner.data.RawDataBank;
import jClassDesigner.data.SplittableConnector_DataWrapper;
import jClassDesigner.data.Variable_DataWrapper;
import jClassDesigner.file.FileManager;
import java.io.IOException;

/**
 *
 * @author Jia Sheng Ma
 * @version 1.1
 */
public class TestSave {
    DataManager dataManager;
    FileManager fileManager;
    RawDataBank dataBank;
    public static final String path = "./work/TestSave.json";

    public TestSave() throws Exception {
        this.dataManager = new DataManager(null);
        this.fileManager = new FileManager(null);
    }
    
    public static void saveThreadExample() {
        ComponentUI_DataWrapper threadExample = new ComponentUI_DataWrapper(Constants.CLASS);
        threadExample.setType(Constants.CLASS);
        threadExample.setParent("Application");
        threadExample.setName("ThreadExample");
        threadExample.setPackageName("");
        // START_TEXT
        Variable_DataWrapper START_TEXT = new Variable_DataWrapper();
        START_TEXT.setName("START_TEXT");
        START_TEXT.setAccess(Constants.PUBLIC);
        START_TEXT.setStatic(true);
        START_TEXT.setType("String");
        threadExample.addVariable(START_TEXT);
        // PAUSE_TEXT
        Variable_DataWrapper PAUSE_TEXT = new Variable_DataWrapper();
        PAUSE_TEXT.setName("PAUSE_TEXT");
        PAUSE_TEXT.setAccess(Constants.PUBLIC);
        PAUSE_TEXT.setStatic(true);
        PAUSE_TEXT.setType("String");
        threadExample.addVariable(PAUSE_TEXT);
        // window
        Variable_DataWrapper window = new Variable_DataWrapper();
        window.setName("window");
        window.setAccess(Constants.PRIVATE);
        window.setStatic(false);
        window.setType("Stage");
        threadExample.addVariable(window);
        // appPane
        Variable_DataWrapper appPane = new Variable_DataWrapper();
        appPane.setName("appPane");
        appPane.setAccess(Constants.PRIVATE);
        appPane.setStatic(false);
        appPane.setType("BorderPane");
        threadExample.addVariable(appPane);
        // startButton
        Variable_DataWrapper startButton = new Variable_DataWrapper();
        startButton.setName("startButton");
        startButton.setAccess(Constants.PRIVATE);
        startButton.setStatic(false);
        startButton.setType("Button");
        threadExample.addVariable(startButton);
        // pauseButton
        Variable_DataWrapper pauseButton = new Variable_DataWrapper();
        pauseButton.setName("pauseButton");
        pauseButton.setAccess(Constants.PRIVATE);
        pauseButton.setStatic(false);
        pauseButton.setType("Button");
        threadExample.addVariable(pauseButton);
        // scrollPane
        Variable_DataWrapper scrollPane = new Variable_DataWrapper();
        scrollPane.setName("scrollPane");
        scrollPane.setAccess(Constants.PRIVATE);
        scrollPane.setStatic(false);
        scrollPane.setType("ScrollPane");
        threadExample.addVariable(scrollPane);
        // textArea
        Variable_DataWrapper textArea = new Variable_DataWrapper();
        textArea.setName("textArea");
        textArea.setAccess(Constants.PRIVATE);
        textArea.setStatic(false);
        textArea.setType("TextArea");
        threadExample.addVariable(textArea);
        // dateThread
        Variable_DataWrapper dateThread = new Variable_DataWrapper();
        dateThread.setName("dateThread");
        dateThread.setAccess(Constants.PRIVATE);
        dateThread.setStatic(false);
        dateThread.setType("Thread");
        threadExample.addVariable(dateThread);
        
        // start method
        Method_DataWrapper start = new Method_DataWrapper();
        start.setAccess(Constants.PUBLIC);
        start.setAbstract(false);
        start.setStatic(false);
        start.setName("start");
        start.setReturnType("void");
        start.addArgument("Stage");
        threadExample.addMethod(start);
        // startWork method
        Method_DataWrapper startWork = new Method_DataWrapper();
        startWork.setAccess(Constants.PUBLIC);
        startWork.setAbstract(false);
        startWork.setStatic(false);
        startWork.setName("startWork");
        startWork.setReturnType("void");
        threadExample.addMethod(startWork);
        // pauseWork method
        Method_DataWrapper pauseWork = new Method_DataWrapper();
        pauseWork.setAccess(Constants.PUBLIC);
        pauseWork.setAbstract(false);
        pauseWork.setStatic(false);
        pauseWork.setName("pauseWork");
        pauseWork.setReturnType("void");
        threadExample.addMethod(pauseWork);
        // doWork method
        Method_DataWrapper doWork = new Method_DataWrapper();
        doWork.setAccess(Constants.PUBLIC);
        doWork.setAbstract(false);
        doWork.setStatic(false);
        doWork.setName("doWork");
        doWork.setReturnType("boolean");
        threadExample.addMethod(doWork);
        // appendText method
        Method_DataWrapper appendText = new Method_DataWrapper();
        appendText.setAccess(Constants.PUBLIC);
        appendText.setAbstract(false);
        appendText.setStatic(false);
        appendText.setName("appendText");
        appendText.setReturnType("void");
        appendText.addArgument("String");
        threadExample.addMethod(appendText);
        // sleep method
        Method_DataWrapper sleep = new Method_DataWrapper();
        sleep.setAccess(Constants.PUBLIC);
        sleep.setAbstract(false);
        sleep.setStatic(false);
        sleep.setName("sleep");
        sleep.setReturnType("void");
        sleep.addArgument("int");
        threadExample.addMethod(sleep);
        // initLayout method
        Method_DataWrapper initLayout = new Method_DataWrapper();
        initLayout.setAccess(Constants.PUBLIC);
        initLayout.setAbstract(false);
        initLayout.setStatic(false);
        initLayout.setName("initLayout");
        initLayout.setReturnType("void");
        threadExample.addMethod(initLayout);
        // initHanlders method
        Method_DataWrapper initHanlders = new Method_DataWrapper();
        initHanlders.setAccess(Constants.PUBLIC);
        initHanlders.setAbstract(false);
        initHanlders.setStatic(false);
        initHanlders.setName("initHanlders");
        initHanlders.setReturnType("void");
        threadExample.addMethod(initHanlders);
        // initWindow method
        Method_DataWrapper initWindow = new Method_DataWrapper();
        initWindow.setAccess(Constants.PUBLIC);
        initWindow.setAbstract(false);
        initWindow.setStatic(false);
        initWindow.setName("initWindow");
        initWindow.setReturnType("void");
        initWindow.addArgument("Stage");
        threadExample.addMethod(initWindow);
        // initThreads method
        Method_DataWrapper initThreads = new Method_DataWrapper();
        initThreads.setAccess(Constants.PUBLIC);
        initThreads.setAbstract(false);
        initThreads.setStatic(false);
        initThreads.setName("initThreads");
        initThreads.setReturnType("void");
        threadExample.addMethod(initThreads);
        // main method
        Method_DataWrapper main = new Method_DataWrapper();
        main.setAccess(Constants.PUBLIC);
        main.setAbstract(false);
        main.setStatic(true);
        main.setName("main");
        main.setReturnType("void");
        main.addArgument("String[]");
        threadExample.addMethod(main);
        
        threadExample.setTranslateX(500);
        threadExample.setTranslateY(100);
        
        // PARENT CONNECTOR
        SplittableConnector_DataWrapper parentConnector = new SplittableConnector_DataWrapper();
        parentConnector.setType(Constants.PARENT_CONNECTOR);
        parentConnector.addAnchor(500, 50);
        parentConnector.addAnchor(500, 100);
        threadExample.addConnector(parentConnector);
        
        // AGGREGATION CONNECTOR TO PAUSE HANDLER
        SplittableConnector_DataWrapper handlerConnector = new SplittableConnector_DataWrapper();
        handlerConnector.setType(Constants.AGGREGATION_CONNECTOR);
        handlerConnector.addAnchor(650, 150);
        handlerConnector.addAnchor(700, 150);
        handlerConnector.addAnchor(700, 100);
        handlerConnector.addAnchor(800, 100);
        threadExample.addConnector(handlerConnector);
        
        RawDataBank.data.add(threadExample);
        
    }
    
    // TEST 2, INTERFACE
    public static void saveThreadExample2() {
        ComponentUI_DataWrapper threadExample = new ComponentUI_DataWrapper(Constants.INTERFACE);
        threadExample.setParent("Application");
        threadExample.setName("ThreadExample");
        threadExample.setPackageName("");
        // START_TEXT
        Variable_DataWrapper START_TEXT = new Variable_DataWrapper();
        START_TEXT.setName("START_TEXT");
        START_TEXT.setAccess(Constants.PUBLIC);
        START_TEXT.setStatic(true);
        START_TEXT.setType("String");
        threadExample.addVariable(START_TEXT);
       
        // start method
        Method_DataWrapper start = new Method_DataWrapper();
        start.setAccess(Constants.PUBLIC);
        start.setAbstract(true);
        start.setStatic(false);
        start.setName("start");
        start.setReturnType("void");
        start.addArgument("Stage");
        threadExample.addMethod(start);
        // startWork method
        Method_DataWrapper startWork = new Method_DataWrapper();
        startWork.setAccess(Constants.PUBLIC);
        startWork.setAbstract(true);
        startWork.setStatic(false);
        startWork.setName("startWork");
        startWork.setReturnType("void");
        threadExample.addMethod(startWork);
        // pauseWork method
        Method_DataWrapper pauseWork = new Method_DataWrapper();
        pauseWork.setAccess(Constants.PUBLIC);
        pauseWork.setAbstract(true);
        pauseWork.setStatic(false);
        pauseWork.setName("pauseWork");
        pauseWork.setReturnType("void");
        threadExample.addMethod(pauseWork);
        // doWork method
        Method_DataWrapper doWork = new Method_DataWrapper();
        doWork.setAccess(Constants.PUBLIC);
        doWork.setAbstract(true);
        doWork.setStatic(false);
        doWork.setName("doWork");
        doWork.setReturnType("boolean");
        threadExample.addMethod(doWork);
        // appendText method
        Method_DataWrapper appendText = new Method_DataWrapper();
        appendText.setAccess(Constants.PUBLIC);
        appendText.setAbstract(true);
        appendText.setStatic(false);
        appendText.setName("appendText");
        appendText.setReturnType("void");
        appendText.addArgument("String");
        threadExample.addMethod(appendText);
        // sleep method
        Method_DataWrapper sleep = new Method_DataWrapper();
        sleep.setAccess(Constants.PUBLIC);
        sleep.setAbstract(true);
        sleep.setStatic(false);
        sleep.setName("sleep");
        sleep.setReturnType("void");
        sleep.addArgument("int");
        threadExample.addMethod(sleep);
        // initLayout method
        Method_DataWrapper initLayout = new Method_DataWrapper();
        initLayout.setAccess(Constants.PUBLIC);
        initLayout.setAbstract(true);
        initLayout.setStatic(false);
        initLayout.setName("initLayout");
        initLayout.setReturnType("void");
        threadExample.addMethod(initLayout);
        // initHanlders method
        Method_DataWrapper initHanlders = new Method_DataWrapper();
        initHanlders.setAccess(Constants.PUBLIC);
        initHanlders.setAbstract(true);
        initHanlders.setStatic(false);
        initHanlders.setName("initHanlders");
        initHanlders.setReturnType("void");
        threadExample.addMethod(initHanlders);
        // initWindow method
        Method_DataWrapper initWindow = new Method_DataWrapper();
        initWindow.setAccess(Constants.PUBLIC);
        initWindow.setAbstract(true);
        initWindow.setStatic(false);
        initWindow.setName("initWindow");
        initWindow.setReturnType("void");
        initWindow.addArgument("Stage");
        threadExample.addMethod(initWindow);
        // initThreads method
        Method_DataWrapper initThreads = new Method_DataWrapper();
        initThreads.setAccess(Constants.PUBLIC);
        initThreads.setAbstract(true);
        initThreads.setStatic(false);
        initThreads.setName("initThreads");
        initThreads.setReturnType("void");
        threadExample.addMethod(initThreads);
        
        threadExample.setTranslateX(500);
        threadExample.setTranslateY(100);
        
        // PARENT CONNECTOR
        SplittableConnector_DataWrapper parentConnector = new SplittableConnector_DataWrapper();
        parentConnector.setType(Constants.PARENT_CONNECTOR);
        parentConnector.addAnchor(500, 50);
        parentConnector.addAnchor(500, 100);
        threadExample.addConnector(parentConnector);
        
        // AGGREGATION CONNECTOR TO PAUSE HANDLER
        SplittableConnector_DataWrapper handlerConnector = new SplittableConnector_DataWrapper();
        handlerConnector.setType(Constants.AGGREGATION_CONNECTOR);
        handlerConnector.addAnchor(450, 150);
        handlerConnector.addAnchor(500, 150);
        handlerConnector.addAnchor(600, 100);
        handlerConnector.addAnchor(700, 100);
        threadExample.addConnector(handlerConnector);
        
        RawDataBank.data.add(threadExample);
        
    }
    
    // TEST 3, ABSTRACT CLASS
    public static void saveThreadExample3() {
        ComponentUI_DataWrapper threadExample = new ComponentUI_DataWrapper(Constants.CLASS);
        threadExample.setParent("Application");
        threadExample.setName("ThreadExample");
        threadExample.setPackageName("");
        // START_TEXT
        Variable_DataWrapper START_TEXT = new Variable_DataWrapper();
        START_TEXT.setName("START_TEXT");
        START_TEXT.setAccess(Constants.PUBLIC);
        START_TEXT.setStatic(true);
        START_TEXT.setType("String");
        threadExample.addVariable(START_TEXT);
       
        // start method
        Method_DataWrapper start = new Method_DataWrapper();
        start.setAccess(Constants.PUBLIC);
        start.setAbstract(true);
        start.setStatic(false);
        start.setName("start");
        start.setReturnType("void");
        start.addArgument("Stage");
        threadExample.addMethod(start);
        // startWork method
        Method_DataWrapper startWork = new Method_DataWrapper();
        startWork.setAccess(Constants.PUBLIC);
        startWork.setAbstract(true);
        startWork.setStatic(false);
        startWork.setName("startWork");
        startWork.setReturnType("void");
        threadExample.addMethod(startWork);
        // pauseWork method
        Method_DataWrapper pauseWork = new Method_DataWrapper();
        pauseWork.setAccess(Constants.PUBLIC);
        pauseWork.setAbstract(true);
        pauseWork.setStatic(false);
        pauseWork.setName("pauseWork");
        pauseWork.setReturnType("void");
        threadExample.addMethod(pauseWork);
        // doWork method
        Method_DataWrapper doWork = new Method_DataWrapper();
        doWork.setAccess(Constants.PUBLIC);
        doWork.setAbstract(true);
        doWork.setStatic(false);
        doWork.setName("doWork");
        doWork.setReturnType("boolean");
        threadExample.addMethod(doWork);
        // appendText method
        Method_DataWrapper appendText = new Method_DataWrapper();
        appendText.setAccess(Constants.PUBLIC);
        appendText.setAbstract(true);
        appendText.setStatic(false);
        appendText.setName("appendText");
        appendText.setReturnType("void");
        appendText.addArgument("String");
        threadExample.addMethod(appendText);
        // sleep method
        Method_DataWrapper sleep = new Method_DataWrapper();
        sleep.setAccess(Constants.PUBLIC);
        sleep.setAbstract(true);
        sleep.setStatic(false);
        sleep.setName("sleep");
        sleep.setReturnType("void");
        sleep.addArgument("int");
        threadExample.addMethod(sleep);
        // initLayout method
        Method_DataWrapper initLayout = new Method_DataWrapper();
        initLayout.setAccess(Constants.PUBLIC);
        initLayout.setAbstract(true);
        initLayout.setStatic(false);
        initLayout.setName("initLayout");
        initLayout.setReturnType("void");
        threadExample.addMethod(initLayout);
        // initHanlders method
        Method_DataWrapper initHanlders = new Method_DataWrapper();
        initHanlders.setAccess(Constants.PUBLIC);
        initHanlders.setAbstract(true);
        initHanlders.setStatic(false);
        initHanlders.setName("initHanlders");
        initHanlders.setReturnType("void");
        threadExample.addMethod(initHanlders);
        // initWindow method
        Method_DataWrapper initWindow = new Method_DataWrapper();
        initWindow.setAccess(Constants.PUBLIC);
        initWindow.setAbstract(true);
        initWindow.setStatic(false);
        initWindow.setName("initWindow");
        initWindow.setReturnType("void");
        initWindow.addArgument("Stage");
        threadExample.addMethod(initWindow);
        // initThreads method
        Method_DataWrapper initThreads = new Method_DataWrapper();
        initThreads.setAccess(Constants.PUBLIC);
        initThreads.setAbstract(true);
        initThreads.setStatic(false);
        initThreads.setName("initThreads");
        initThreads.setReturnType("void");
        threadExample.addMethod(initThreads);
        
        threadExample.setTranslateX(200);
        threadExample.setTranslateY(300);
        
        // PARENT CONNECTOR
        SplittableConnector_DataWrapper parentConnector = new SplittableConnector_DataWrapper();
        parentConnector.setType(Constants.PARENT_CONNECTOR);
        parentConnector.addAnchor(50, 50);
        parentConnector.addAnchor(50, 100);
        threadExample.addConnector(parentConnector);
        
        // AGGREGATION CONNECTOR TO PAUSE HANDLER
        SplittableConnector_DataWrapper handlerConnector = new SplittableConnector_DataWrapper();
        handlerConnector.setType(Constants.AGGREGATION_CONNECTOR);
        handlerConnector.addAnchor(650, 150);
        handlerConnector.addAnchor(700, 150);
        handlerConnector.addAnchor(700, 100);
        handlerConnector.addAnchor(800, 100);
        threadExample.addConnector(handlerConnector);
        
        RawDataBank.data.add(threadExample);
        
    }
    
    public static void savePauseHanlder() {
        ComponentUI_DataWrapper pauseHanlder = new ComponentUI_DataWrapper(Constants.CLASS);
        pauseHanlder.setParent("EventHandler");
        pauseHanlder.setName("PauseHanlder");
        pauseHanlder.setPackageName("");
        
        // app var
        Variable_DataWrapper app = new Variable_DataWrapper();
        app.setAccess(Constants.PRIVATE);
        app.setName("app");
        app.setStatic(false);
        app.setType("ThreadExample");
        pauseHanlder.addVariable(app);
        // constructor
        Method_DataWrapper pauseHandler_constructor = new Method_DataWrapper();
        pauseHandler_constructor.setAccess(Constants.PUBLIC);
        pauseHandler_constructor.setAbstract(false);
        pauseHandler_constructor.setStatic(false);
        pauseHandler_constructor.setName("PauseHanlder");
        pauseHandler_constructor.setReturnType("");
        pauseHandler_constructor.addArgument("ThreadExample");
        pauseHanlder.addMethod(pauseHandler_constructor);
        // handle method
        Method_DataWrapper handle = new Method_DataWrapper();
        handle.setAccess(Constants.PUBLIC);
        handle.setAbstract(false);
        handle.setStatic(false);
        handle.setName("handle");
        handle.setReturnType("void");
        handle.addArgument("Event");
        pauseHanlder.addMethod(handle);
        
        pauseHanlder.setTranslateX(650);
        pauseHanlder.setTranslateY(150);
        
        RawDataBank.data.add(pauseHanlder);

    }
    
    public static void saveStartHanlder() {
        ComponentUI_DataWrapper startHanlder = new ComponentUI_DataWrapper(Constants.CLASS);
        startHanlder.setParent("EventHandler");
        startHanlder.setName("StartHanlder");
        startHanlder.setPackageName("");
        
        // app var
        Variable_DataWrapper app = new Variable_DataWrapper();
        app.setAccess(Constants.PRIVATE);
        app.setName("app");
        app.setStatic(false);
        app.setType("ThreadExample");
        startHanlder.addVariable(app);
        // constructor
        Method_DataWrapper startHandler_constructor = new Method_DataWrapper();
        startHandler_constructor.setAccess(Constants.PUBLIC);
        startHandler_constructor.setAbstract(false);
        startHandler_constructor.setStatic(false);
        startHandler_constructor.setName("StartHanlder");
        startHandler_constructor.setReturnType("");
        startHandler_constructor.addArgument("ThreadExample");
        startHanlder.addMethod(startHandler_constructor);
        // handle method
        Method_DataWrapper handle = new Method_DataWrapper();
        handle.setAccess(Constants.PUBLIC);
        handle.setAbstract(false);
        handle.setStatic(false);
        handle.setName("handle");
        handle.setReturnType("void");
        handle.addArgument("Event");
        startHanlder.addMethod(handle);
        
        startHanlder.setTranslateX(650);
        startHanlder.setTranslateY(300);
        
        RawDataBank.data.add(startHanlder);

    }
    
    public static void saveCounterTask() {
        ComponentUI_DataWrapper counterTask = new ComponentUI_DataWrapper(Constants.CLASS);
        counterTask.setParent("Task");
        counterTask.setName("CounterTask");
        counterTask.setPackageName("");
        
        // app var
        Variable_DataWrapper app = new Variable_DataWrapper();
        app.setAccess(Constants.PRIVATE);
        app.setName("app");
        app.setStatic(false);
        app.setType("ThreadExample");
        counterTask.addVariable(app);
        // counter var
        Variable_DataWrapper counter = new Variable_DataWrapper();
        counter.setAccess(Constants.PRIVATE);
        counter.setName("counter");
        counter.setStatic(false);
        counter.setType("int");
        counterTask.addVariable(counter);
		
        // constructor
        Method_DataWrapper counterTask_constructor = new Method_DataWrapper();
        counterTask_constructor.setAccess(Constants.PUBLIC);
        counterTask_constructor.setAbstract(false);
        counterTask_constructor.setStatic(false);
        counterTask_constructor.setName("CounterTask");
        counterTask_constructor.setReturnType("");
        counterTask_constructor.addArgument("ThreadExample");
        counterTask.addMethod(counterTask_constructor);
        // handle method
        Method_DataWrapper call = new Method_DataWrapper();
        call.setAccess(Constants.PROTECTED);
        call.setAbstract(false);
        call.setStatic(false);
        call.setName("call");
        call.setReturnType("void");
        counterTask.addMethod(call);
        
        counterTask.setTranslateX(100);
        counterTask.setTranslateY(150);
        
        RawDataBank.data.add(counterTask);
        
    }
    
    public static void saveDateTask() {
        ComponentUI_DataWrapper dateTask = new ComponentUI_DataWrapper(Constants.CLASS);
        dateTask.setParent("Task");
        dateTask.setName("DateTask");
        dateTask.setPackageName("");
        
        // app var
        Variable_DataWrapper app = new Variable_DataWrapper();
        app.setAccess(Constants.PRIVATE);
        app.setName("app");
        app.setStatic(false);
        app.setType("ThreadExample");
        dateTask.addVariable(app);
        // date var
        Variable_DataWrapper date = new Variable_DataWrapper();
        date.setAccess(Constants.PRIVATE);
        date.setName("now");
        date.setStatic(false);
        date.setType("Date");
        dateTask.addVariable(date);
		
        // constructor
        Method_DataWrapper dateTask_constructor = new Method_DataWrapper();
        dateTask_constructor.setAccess(Constants.PUBLIC);
        dateTask_constructor.setAbstract(false);
        dateTask_constructor.setStatic(false);
        dateTask_constructor.setName("DateTask");
        dateTask_constructor.setReturnType("");
        dateTask_constructor.addArgument("ThreadExample");
        dateTask.addMethod(dateTask_constructor);
        // handle method
        Method_DataWrapper call = new Method_DataWrapper();
        call.setAccess(Constants.PROTECTED);
        call.setAbstract(false);
        call.setStatic(false);
        call.setName("call");
        call.setReturnType("void");
        dateTask.addMethod(call);
        
        dateTask.setTranslateX(100);
        dateTask.setTranslateY(350);
        
        RawDataBank.data.add(dateTask);
        
    }
    
    public DataManager getDataManager() {
        return dataManager;
    }
    
    public void saveData(String dst) throws IOException {
        fileManager.saveData(dataManager, dst);
    }
    
    public static void main(String[] args) throws IOException, Exception{
        TestSave t = new TestSave();
        FileManager f = t.fileManager;
        DataManager d = t.dataManager;
                
        // save data
        saveThreadExample();
        savePauseHanlder();
        saveStartHanlder();
        saveCounterTask();
        saveDateTask();
        
        t.saveData(path);
        System.out.println(" >> Data saved successfully to: " + path);
    }   
    
}