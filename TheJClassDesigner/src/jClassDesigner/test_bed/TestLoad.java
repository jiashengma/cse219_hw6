package jClassDesigner.test_bed;

import jClassDesigner.JClassDesigner;
import jClassDesigner.data.DataManager;
import jClassDesigner.file.FileManager;
import java.io.IOException;

/**
 *
 * @author Jia Sheng Ma
 */
public class TestLoad {
    JClassDesigner jcd;
    TestSave testSave;
    DataManager dataManager;
    FileManager fileManager;

    public TestLoad() throws Exception {
        testSave = new TestSave();
        dataManager = testSave.getDataManager();
        fileManager = new FileManager(null);
        
    }
    
    public void loadData(String dir) {
        
        try {
            System.out.println("Start loading...");
            fileManager.loadData(dataManager, dir);
            System.out.println("Loading succecced from: " + dir);
        } catch(IOException ioe) {
            System.out.println("Unable to load file");
        }
        
    }
    
    public static void main(String[] args) throws Exception {
        TestLoad t = new TestLoad();
        
        t.loadData(TestSave.path);
        
    }
}
