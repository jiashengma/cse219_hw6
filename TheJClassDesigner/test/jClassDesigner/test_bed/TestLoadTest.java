/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jClassDesigner.test_bed;

import jClassDesigner.data.RawDataBank;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jia Sheng Ma
 */

public class TestLoadTest/* extends Application*/{
    
    
    public TestLoadTest() throws Exception {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("* TestLoad JUnit Test: @Before Class method");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("* TestLoad JUnit Test: @After Class method");
        
    }

    /**
     * Test of ThreadExample 1.
     */
    @Test
    public void testEqual() throws Exception {
        TestLoad t = new TestLoad();
        // assuming json is already there
        t.loadData("./work/TestSave.json");
        
        assertEquals(5, RawDataBank.data.size());                                       // assert total number of class saved
        assertEquals("class", RawDataBank.data.get(0).getType());
        assertEquals("ThreadExample", RawDataBank.data.get(0).getName());
        assertEquals("START_TEXT", RawDataBank.data.get(0).getVariables().get(0).getName());
        assertEquals("String", RawDataBank.data.get(0).getVariables().get(0).getType());
        assertEquals(500 , RawDataBank.data.get(0).getTranslateX(), 0);                 // assert first class' diagram's x
        assertEquals(100 , RawDataBank.data.get(0).getTranslateY(), 0);                 // assert first class' diagram's y
        assertEquals("call", RawDataBank.data.get(4).getMethods().get(1).getName());    // assert last method 
        assertEquals("EventHandler", RawDataBank.data.get(1).getExtendedParent());              // assert second class'parent
        assertEquals("class", RawDataBank.data.get(1).getType());
        assertEquals(500, RawDataBank.data.get(0).getConnectors().get(0).getAnchorXes().get(0),0);  // assert the first connector's x pos
    }
    
    /**
     * Test of ThreadExample 2.
     * @throws Exception 
     */
    @Test
    public void testEqual2() throws Exception {
        String path = "./work/Test2.json";
        TestSave s = new TestSave();
        
        // clear data from previous test
        RawDataBank.data.clear();
        // save only one design, an interface
        TestSave.saveThreadExample2();
        s.saveData(path);
        TestLoad l = new TestLoad();
        l.loadData(path);
        
        // assert total number of class saved
        assertEquals(1, RawDataBank.data.size());            
        
        assertEquals("interface", RawDataBank.data.get(0).getType());
        
        // assert static variable in interface
        assertEquals(true, RawDataBank.data.get(0).getVariables().get(0).getVariableStatic());
        
        // assert abstract class' abstract method
        assertEquals(true, RawDataBank.data.get(0).getMethods().get(0).getAbstract());  
        
        assertEquals("START_TEXT", RawDataBank.data.get(0).getVariables().get(0).getName());
        
        assertEquals("String", RawDataBank.data.get(0).getVariables().get(0).getType());
        
        // assert diagram's x
        assertEquals(500 , RawDataBank.data.get(0).getTranslateX(), 0);                 
        
        // assert' diagram's y
        assertEquals(100 , RawDataBank.data.get(0).getTranslateY(), 0);
        
        // assert first connector's x
        assertEquals(500, RawDataBank.data.get(0).getConnectors().get(0).getAnchorXes().get(0),0);
        
        // assert the 2 connector's 1st x pos
        assertEquals(450, RawDataBank.data.get(0).getConnectors().get(1).getAnchorXes().get(0),0);  
        
    }
    /**
     * Test of ThreadExample 3.
     * @throws Exception 
     */
    @Test
    public void testEqual3() throws Exception {
        String path = "./work/Test3.json";
        TestSave s = new TestSave();
        
        // clear data from previous test
        RawDataBank.data.clear();
        // save only one design, an interface
        TestSave.saveThreadExample3();
        
        s.saveData(path);
        TestLoad l = new TestLoad();
        l.loadData(path);
        
        // assert total number of class saved
        assertEquals(1, RawDataBank.data.size());            
        
        assertEquals("class", RawDataBank.data.get(0).getType());
        
        // assert if it is abstract method
        assertEquals(true, RawDataBank.data.get(0).getMethods().get(0).getAbstract());  
        
        // assert abstrac method name
        assertEquals("start", RawDataBank.data.get(0).getMethods().get(0).getName());
        
        // assert if variable is static 
        assertEquals("String", RawDataBank.data.get(0).getVariables().get(0).getType());
        
        // assert diagram's x
        assertEquals(200 , RawDataBank.data.get(0).getTranslateX(), 0);                 
        
        // assert' diagram's y
        assertEquals(300 , RawDataBank.data.get(0).getTranslateY(), 0);
        
        // assert first connector's x
        assertEquals(50, RawDataBank.data.get(0).getConnectors().get(0).getAnchorXes().get(0),0);
        // assert first connector's y
        assertEquals(50, RawDataBank.data.get(0).getConnectors().get(0).getAnchorYs().get(0),0);
        
        // assert the 2 connector's 1st x pos
        assertEquals(650, RawDataBank.data.get(0).getConnectors().get(1).getAnchorXes().get(0),0);  
    }
    
}